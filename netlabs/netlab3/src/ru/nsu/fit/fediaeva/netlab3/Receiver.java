package ru.nsu.fit.fediaeva.netlab3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Receiver implements Runnable {
    private InetSocketAddress address;
    private ConcurrentLinkedQueue<Message> messages;
    private final ArrayList<InetSocketAddress> connections;
    private int lostPercent;

    Receiver(ConcurrentLinkedQueue<Message> receivedMsg,
             InetSocketAddress adr,
             ArrayList<InetSocketAddress> connections, int lostPercent) {
        address = adr;
        this.messages = receivedMsg;
        this.connections = connections;
        this.lostPercent = lostPercent;
    }

    private void addMsg(Message message) {
        switch (message.getType()) {
            case "ACK":
                messages.removeIf(m -> {
                    if (m.getGuid().equals(UUID.fromString(message.getData()))) {
                        if (m.getType().equals("REQUEST")) {
                            System.out.println("Connection confirmed by " + m.getReceiver());
                        }
                        return true;
                    }
                    return false;
                });
                break;
            case "REQUEST":
                if (containsMsg(message)) return;
                messages.add(message);
                synchronized (connections) {
                    connections.add(message.getSender());
                    System.out.println("Added connection : " + message.getSender());
                }
                messages.add(generateAck(message));
                break;
            default:
                if (containsMsg(message)) return;
                messages.add(message);
                System.out.println("RECEIVED : " + message.getData());
                synchronized (connections) {
                    for (InetSocketAddress adr : connections) {
                        if (!adr.equals(message.getSender())) {
                            messages.add(new Message(message.getData(), message.getType(), address, adr, UUID.randomUUID()));
                        } else {
                            messages.add(generateAck(message));
                        }
                    }
                }
                break;
        }
    }

    private Message generateAck(Message message) {
        return new Message(message.getGuid().toString(), "ACK", address, message.getSender(), UUID.randomUUID());
    }

    private boolean containsMsg(Message m){
        for (Message msg : messages) {
            if (msg.getGuid().equals(m.getGuid())){
                msg.setLastReceived(System.currentTimeMillis());
                messages.add(generateAck(m));
                return true;
            }
        }
        return false;
    }

    @Override
    public void run() {
        try {
            byte[] buf = new byte[1024];
            DatagramSocket socket = new DatagramSocket(address);
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            Message m;
            Random rnd = new Random(System.currentTimeMillis());
            while (true) {
                socket.receive(packet);
                if (rnd.nextInt(100) < lostPercent){
                    continue;
                }
                String data = new String(buf, 0, packet.getLength(), "UTF-8");
                String[] msgParts = data.split(":", 4);
                String type = msgParts[0];
                String guid = msgParts[1];
                int port = Integer.parseInt(msgParts[2]);
                String msgContent = msgParts[3];
                InetSocketAddress sender = new InetSocketAddress(packet.getAddress(), port);
                m = new Message(msgContent, type, sender, address, UUID.fromString(guid));
                addMsg(m);
            }
        } catch (SocketException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
