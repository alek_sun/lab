package ru.nsu.fit.fediaeva.netlabs.httpClient;

import java.io.*;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

public class ConsoleInputReadTask implements Callable<String> {
    private AtomicBoolean isStopped;

    ConsoleInputReadTask(AtomicBoolean isStopped) {
        this.isStopped = isStopped;
    }

    public String call() throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));
        String input;
        do {
            try {
                while (!br.ready()) {
                    if (isStopped.get()) return null;
                    Thread.sleep(500);
                }
                if (isStopped.get()) return null;
                input = br.readLine();
            } catch (InterruptedException e) {
                System.out.println("ConsoleInputReadTask cancelled");
                return null;
            }
        } while (input.equals(""));
        return input;
    }
}