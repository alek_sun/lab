package ru.nsu.fit.fediaeva.netlab4server.matchers;

public interface Matcher {
    boolean match(String str);
}
