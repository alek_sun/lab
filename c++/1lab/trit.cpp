#include "trit.h"
#include <iostream>
#include <iterator>
#include <cstring>

using std::cout;
using std::cin;
using std::endl;

namespace Lab1 {

    //  TRITSET   //--------------------------------------------------------------------------------------------
    //     Constructors  // ----------------------------------------------------------------------------

    TritSet::TritSet() : num2B(ZERO), countTRUE(ZERO), countFALSE(ZERO), lastFixInd(-1), arrSize(ZERO) {
        cont2B = new uint[ZERO]();
    }

    TritSet::TritSet(int size) : num2B(ZERO), countTRUE(ZERO), countFALSE(ZERO), lastFixInd(-1), arrSize(ZERO) {
        if (size < ZERO) {
            throw std::runtime_error("size < 0");
        }
        resize(size);
        int allocLength = capacity(); // count of avalible bytes
        if (allocLength < size / SECT2B) {
            throw std::length_error("Out of range at TritSet");
        }
    }

    TritSet::TritSet(const TritSet &b) : num2B(b.num2B), countTRUE(b.countTRUE), countFALSE(b.countFALSE),
                                         lastFixInd(b.lastFixInd), arrSize(b.arrSize) {
        cont2B = new uint[arrSize]();
        for (int i = ZERO; i < arrSize; i++) {
            cont2B[i] = b.cont2B[i];
        }
    }

    //  Destructors  //---------------------------------------------------------------------------------

    TritSet::~TritSet() {
        delete[] cont2B;
    }

    //     Methods    //-------------------------------------------------------------------------------

    void TritSet::resize(int newNum2B) {
        int oldSize = (num2B + SECT2B - ONE) / SECT2B;
        int newSize = (newNum2B + SECT2B - ONE) / SECT2B;
        uint *newCont2B = new uint[newSize]();
        int small;
        if (newSize < oldSize) {
            small = newSize;
        } else {
            small = oldSize;
        }
        if (oldSize != ZERO) {
            for (int i = ZERO; i < small; i++) {
                newCont2B[i] = cont2B[i];
            }
            delete[] cont2B;
        }
        cont2B = new uint[newSize]();
        for (int i = ZERO; i < newSize; i++) {
            cont2B[i] = newCont2B[i];
        }
        delete[] newCont2B;
        num2B = newNum2B;
        arrSize = (newNum2B + SECT2B - ONE) / SECT2B;
    }

    void TritSet::set(int trIndex, TritSet::Trit value) {
        if (trIndex < ZERO) {
            throw std::runtime_error("index < 0");
        }
        if (value == TritSet::Trit::UNKNOWN) {
            if (trIndex > lastFixInd) return;
            if (trIndex == lastFixInd) { // change lastFixInd if lastFix became UNKNOWN
                int i = lastFixInd - ONE;
                while (i >= ZERO && get(i) == TritSet::Trit::UNKNOWN) {
                    i--;
                }
                lastFixInd = i;
            }
        } else {
            if (trIndex >= num2B)
                resize(trIndex + ONE); // +1 because index beginning from 0, but resize for count
            if (trIndex > lastFixInd) {
                lastFixInd = trIndex;
            }
        }
        if (get(trIndex) == TritSet::Trit::TRUE) {
            countTRUE--;
        }
        if (get(trIndex) == TritSet::Trit::FALSE) {
            countFALSE--;
        }
        int newVal = ZERO;
        if (value == TritSet::Trit::TRUE) {
            newVal = 2;
            countTRUE++;
        }
        if (value == TritSet::Trit::FALSE) {
            newVal = 1;
            countFALSE++;
        }

        uint buf = cont2B[trIndex / SECT2B]; // cur uint
        int rem = trIndex % RADIX16 + ONE; // +1 for cur
        int shift = BIT2 * (RADIX16 - rem);
        uint cur = buf >> shift;
        cur %= RADIX4;
        cur <<= shift;
        buf -= cur;
        newVal <<= shift;
        cont2B[trIndex / SECT2B] = buf + newVal;
    }

    TritSet::Trit TritSet::get(int trIndex) const {
        if (trIndex < ZERO) {
            throw std::runtime_error("index < 0"); //UNKNOWNIND;
        }
        if (trIndex >= num2B) {
            return TritSet::Trit::UNKNOWN;
        }
        int rem = trIndex % RADIX16 + ONE; // +1 for cur
        int shift = BIT2 * (RADIX16 - rem);
        uint cur = cont2B[trIndex / SECT2B];
        cur >>= shift;
        cur %= RADIX4;
        switch (cur) {
            case 1: {
                return TritSet::Trit::FALSE;
            }
            case 2: {
                return TritSet::Trit::TRUE;
            }
            default: {
                return TritSet::Trit::UNKNOWN;
            }
        }
    }

    TritSet::Trit TritSet::handleBinaryOperation(int type, TritSet::Trit set1, TritSet::Trit set2) {
        switch (type) {
            case AND: {
                if (set1 == TritSet::Trit::FALSE || set2 == TritSet::Trit::FALSE) {
                    return TritSet::Trit::FALSE;
                }
                if (set1 == TritSet::Trit::UNKNOWN || set2 == TritSet::Trit::UNKNOWN) {
                    return TritSet::Trit::UNKNOWN;
                }
                return TritSet::Trit::TRUE;
            }
            case OR: {
                if (set1 == TritSet::Trit::TRUE || set2 == TritSet::Trit::TRUE) {
                    return TritSet::Trit::TRUE;
                }
                if (set1 == TritSet::Trit::UNKNOWN || set2 == TritSet::Trit::UNKNOWN) {
                    return TritSet::Trit::UNKNOWN;
                }
                return TritSet::Trit::FALSE;
            }
            default:
                return TritSet::Trit::UNKNOWN;
        }
    }


    TritSet::Trit TritSet::notOperation(TritSet::Trit set) {
        if (set == TritSet::Trit::TRUE) {
            return TritSet::Trit::FALSE;
        }
        if (set == TritSet::Trit::FALSE) {
            return TritSet::Trit::TRUE;
        }
        return TritSet::Trit::UNKNOWN;
    }

    TritSet TritSet::logic(int type, TritSet &set2) {
        TritSet& set1 = *this;
        int maxsize = set2.num2B > set1.num2B ? set2.num2B : set1.num2B;
        TritSet res(maxsize);
        if (type == NOT){
            for (int i = ZERO; i < maxsize; i++) {
                res[i] = notOperation(set1[i]);
            }
            return res;
        }
        for (int i = ZERO; i < maxsize; i++) {
            res[i] = handleBinaryOperation(type, set1[i], set2[i]);
        }
        return res;
    }

    int TritSet::length() {
        return lastFixInd + ONE;
    }

    int TritSet::capacity() {
        return arrSize * sizeof(uint); //  byte in cont2B
    }

    int TritSet::trueCount() {
        return countTRUE;
    }

    int TritSet::falseCount() {
        return countFALSE;
    }

    int TritSet::unkCount() {
        int count = length() - countFALSE - countTRUE;
        return count;
    }

    void TritSet::trim(int lastInd) {
        int i;
        for (i = num2B - ONE; i >= lastInd; i--) {
            set(i, TritSet::Trit::UNKNOWN);
        }
        resize(lastInd);
    }

    void TritSet::shrink() {
        resize(length());
    }

    TritSet::TritIterator TritSet::begin() {
        return TritIterator(*this, 0);
    }

    TritSet::TritIterator TritSet::end() {
        return TritIterator(*this, length());
    }

    int TritSet::cardinality(Trit el) {
        switch (el) {
            case TritSet::Trit::TRUE:
                return trueCount();
            case TritSet::Trit::FALSE:
                return falseCount();
            default:
                return unkCount();
        }
    }

    TritSet::TrMap TritSet::cardinality() {
        TrMap map({
              {TritSet::Trit::TRUE,  cardinality(TritSet::Trit::TRUE)},
              {TritSet::Trit::FALSE,  cardinality(TritSet::Trit::FALSE)},
              {TritSet::Trit::UNKNOWN, cardinality(TritSet::Trit::UNKNOWN)}
          });
        return map;
    }

    void TritSet::saveToStream(std::ostream& out){
        cout << "T|" << num2B << "|";
        for (int i = ZERO; i < arrSize; i++) {
            out << cont2B[i] << " ";
        }
    }

    void TritSet::loadFromStream(std::istream& in){
        char bufmagic = '0';
        in >> bufmagic;
        if (bufmagic != 'T'){
            throw std::domain_error("not Tritset");
        }
        cin.ignore(1, '|');
        in >> num2B;
        resize(num2B);
        in.ignore(1, '|');
        uint buf = ZERO;
        for (int i = ZERO; i <= num2B/SECT2B; i++){
            in >> buf;
            cont2B[i] = buf;
        }
    }

    //   Operators    //------------------------------------------------------------------

    TritSet::TritHolder TritSet::operator[](int index) {
        return TritHolder(*this, index);
    }

    TritSet::Trit TritSet::operator[](int index) const {
        return get(index);
    }

    TritSet& TritSet::operator=(const TritSet &newEl) {  // copy operator
        if (this == &newEl) {
            return *this;
        }
        resize(newEl.num2B);
        for (int i = ZERO; i < arrSize; i++) {
            cont2B[i] = newEl.cont2B[i];
        }
        num2B = newEl.num2B;
        arrSize = newEl.arrSize;
        countTRUE = newEl.countTRUE;
        countFALSE = newEl.countFALSE;
        lastFixInd = newEl.lastFixInd;
        return *this;
    }

    //  Logic operators //--------------------------------------------------------------------------------------

    TritSet TritSet::operator&(TritSet& other) {
        return logic(AND, other);
    }

    TritSet TritSet::operator|(TritSet& other) {
        return logic(OR, other);
    }

    TritSet TritSet::operator!() {
        return logic(NOT, *this);
    }


    //  TRIT HOLDER // ------------------------------------------------------------------------------------------------
    //  Constructor   //-------------------------------------------------------------------------------------------------
    TritSet::TritHolder::TritHolder(TritSet &basic, int index) : rootCont(basic), ind(index) {};

    //   Operators  //---------------------------------------------------------------------------------------
    TritSet::Trit TritSet::TritHolder::operator=(TritSet::TritHolder value) {
        if (ind > rootCont.num2B && value.rootCont.get(ind) == TritSet::Trit::UNKNOWN) {
            return TritSet::Trit::UNKNOWN;
        }
        rootCont.set(ind, value);
        return value;
    }

    TritSet::Trit TritSet::TritHolder::operator=(TritSet::Trit value) {
        if (value == TritSet::Trit::UNKNOWN && ind > rootCont.num2B) {
            return TritSet::Trit::UNKNOWN;
        }
        rootCont.set(ind, value);
        return value;
    }

    TritSet::TritHolder::operator TritSet::Trit() const {
        return rootCont.get(ind);
    }
    TritSet::Trit TritSet::TritHolder::operator&(TritSet::Trit other){
        TritSet::Trit el1 = rootCont[ind];
        TritSet::Trit el2 = other;
        TritSet::Trit res = handleBinaryOperation(AND, el1, el2);
        return res;
    }
    TritSet::Trit TritSet::TritHolder::operator|(TritSet::Trit other){
        TritSet::Trit el1 = rootCont[ind];
        TritSet::Trit el2 = other;
        TritSet::Trit res = handleBinaryOperation(OR, el1, el2);
        return res;
    }
    TritSet::Trit TritSet::TritHolder::operator!(){
        TritSet::Trit res = notOperation(rootCont[ind]);
        return res;
    }


    //     ITERATOR  //--------------------------------------------------------------------------------------------------

    TritSet::TritIterator::TritIterator(TritSet &rootSet, int index) : set(rootSet), ind(index) {}

    TritSet::TritHolder TritSet::TritIterator::operator*() {
        return set[ind];
    }

    TritSet::TritIterator &TritSet::TritIterator::operator++() {
        ind++;
        return *this;
    }

    TritSet::TritIterator &TritSet::TritIterator::operator++(int) {
        ind++;
        return *this;
    }

    bool TritSet::TritIterator::operator==(TritSet::TritIterator other) {
        return ind == other.ind;
    }

    bool TritSet::TritIterator::operator!=(TritSet::TritIterator other) {
        return ind != other.ind;
    }

    //      CONSTITERATOR    // --------------------------------------------------------------------------------------------------

    TritSet::CTritIterator::CTritIterator(TritSet &rootSet, int index) : set(rootSet), ind(index) {}

    TritSet::TritHolder TritSet::CTritIterator::operator*() const {
        return set[ind];
    }

    TritSet::CTritIterator& TritSet::CTritIterator::operator++() { // prefix
        ind++;
        return *this;
    }

    TritSet::CTritIterator& TritSet::CTritIterator::operator++(int) { // postfix
        ind++;
        return *this;
    }

    bool TritSet::CTritIterator::operator==(TritSet::CTritIterator other) {
        return ind == other.ind;
    }

    bool TritSet::CTritIterator::operator!=(TritSet::CTritIterator other) {
        return ind != other.ind;
    }

    //  UINTTRIT  // -------------------------------------------------------------------------------------------------

    TritSet::UintTrit::UintTrit() {}

    TritSet::uint TritSet::UintTrit::operator()(const TritSet::Trit &el) const {
        switch (el) {
            case TritSet::Trit::TRUE:
                return 2;
            case TritSet::Trit::FALSE:
                return 1;
            default:
                return 0;
        }
    }
}
