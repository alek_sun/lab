package ru.nsu.fit.fediaeva.netlab4server.requesthandlers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.nsu.fit.fediaeva.netlab4server.HttpMessage;
import ru.nsu.fit.fediaeva.netlab4server.Message;
import ru.nsu.fit.fediaeva.netlab4server.ServerException;

import java.util.ArrayList;

public class MessagesGetter implements RequestHandler {
    private final ArrayList<Message> messages;

    public MessagesGetter(ArrayList<Message> messages) {
        this.messages = messages;
    }

    @Override
    public HttpMessage getResponse(HttpMessage request) throws ServerException {
        JSONObject respBody = new JSONObject();
        JSONArray msgArr = new JSONArray();
        synchronized (messages) {
            messages.forEach(message -> createMsgObj(msgArr, message));
        }
        respBody.put("messages", msgArr);

        HttpMessage response = new HttpMessage();
        response.setOKTitle();
        response.getHeaders().put("Content-Type", "application/json");
        response.setBody(respBody.toJSONString());
        return response;
    }

    static void createMsgObj(JSONArray msgArr, Message msg) {
        JSONObject msgObj = new JSONObject();
        msgObj.put("id", msg.getId());
        msgObj.put("message", msg.getData());
        msgObj.put(("author"), msg.getSenderId());
        msgArr.add(msgObj);
    }
}
