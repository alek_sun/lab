package ru.nsu.fit.fediaeva.netlabs.netlab5;

import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;

class WebSocketClient {
    private static CountDownLatch messageLatch;
    private ArrayBlockingQueue<String> messages;
    private Session userSession;
    private MessageHandler messageHandler;

    WebSocketClient(){
        /*try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(this, endpointURI);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }*/
        messages = new ArrayBlockingQueue<>(20);
        messageLatch = new CountDownLatch(1);
    }


    void start(){
        ChatClientEndpoint clientEndpoint = new ChatClientEndpoint(messageLatch, messages);
        /*final ClientEndpointConfig config = ClientEndpointConfig.Builder.create().build();
        ClientManager client = ClientManager.createClient();

        try {
            Session s = client.connectToServer(ChatClientEndpoint.class, config,
                    new URI("ws://localhost:8025/websockets/login"));
            System.out.println("Main " + s.getId());
        } catch (URISyntaxException | IOException | DeploymentException e) {
            e.printStackTrace();
        }*/
        try {
            String message;
            ConsoleInputReadTask scanner = new ConsoleInputReadTask(messageLatch);

            while ((message = scanner.call()) != null) {
                messages.offer(message);
            }
            //messageLatch.await();////////////////////
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
