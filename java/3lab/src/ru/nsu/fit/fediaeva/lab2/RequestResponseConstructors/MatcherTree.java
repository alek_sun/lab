package ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors;

import ru.nsu.fit.fediaeva.lab2.Exception.NotFound;
import ru.nsu.fit.fediaeva.lab2.HttpHandler.HttpHandler;
import ru.nsu.fit.fediaeva.lab2.Matchers.SegmentMatcher;
import ru.nsu.fit.fediaeva.lab2.Matchers.StringMatcher;

import java.util.*;

/**
 * Tree which nodes are SegmentMatcher's and HttpHandler's.
 * HttpHandler's is equal with null everywhere besides tree leaves.
 * Each node has a list of branches
 */
public class MatcherTree {
    private SegmentMatcher segMatcher;
    private ArrayList<MatcherTree> branches;
    private HttpHandler handler;

    public MatcherTree() {
        segMatcher = new StringMatcher("root", null);
        branches = new ArrayList<>();
        handler = null;
    }

    private MatcherTree(SegmentMatcher s) {
        segMatcher = s;
        branches = new ArrayList<>();
        handler = null;
    }

    /**
     * Find equal tree node by SegmentMatcher
     * Used for filling tree.
     * @param s SegmentMatcher for which looking for equal matcher
     * @return  Tree node with equal matcher
     */
    private MatcherTree getEqualNode(SegmentMatcher s) {
        for (MatcherTree t : branches) {
            if (t.segMatcher.equals(s)) {
                return t;
            }
        }
        return null;
    }

    /**
     * Register HttpHandler by path
     * @param l Register path template as SegmentMatcher's
     * @param h Registered HttpHandler
     */
    public void register(ArrayList<SegmentMatcher> l, HttpHandler h) {
        SegmentMatcher s = l.remove(0);
        if (s == null) {
            handler = h;
            return;
        }
        MatcherTree equal = getEqualNode(s);
        if (equal == null) {
            branches.add(new MatcherTree(s));
            branches.get(branches.size() - 1).register(l, h);
        } else {
            getActualDescription(s, equal);
            equal.register(l, h);
        }
    }

    /**
     * Replace not actual description in SegmentMatcher, if in the new list of SegmentMatcher's came actual information
     * @param s Matcher with actual description
     * @param equal Redacted tree node
     */
    private void getActualDescription(SegmentMatcher s, MatcherTree equal) {
        String newDescr = s.getDescription();
        SegmentMatcher oldMatcher = equal.getMatcher();
        if (!oldMatcher
                .getDescription()
                .equals(newDescr)) {
            oldMatcher.setDescr(newDescr);
        }
    }

    /**
     * Get HttpHandler from tree by path
     * @param pathArr Path
     * @param info Map for path information
     * @return  HttpHandler
     * @throws NotFound
     * Throws :
     *  *    if came incorrect path (longer, than tree branch)
     *  *    if handler not registered
     *  *    if registered null-handler
     */
    public HttpHandler getHandler(ArrayList<String> pathArr, Map<String, String> info) throws NotFound {
        String segment = pathArr.remove(0);
        if (segment == null) {
            if (handler == null) {    //  if found handler for directory, or someone register null handler
                throw new NotFound();
            } else {
                return handler;
            }
        }
        if (branches.isEmpty()) {   //  if handler for this remainder of this path is absent
            throw new NotFound();
        }
        MatcherTree candidate = null;
        for (MatcherTree t : branches) {
            SegmentMatcher m = t.getMatcher();
            if (m.match(segment, info)) {
                if (m.getPriority() == 0) {
                    candidate = t;
                    continue;
                }
                return t.getHandler(pathArr, info);
            }
        }
        if (candidate != null) {
            return candidate.getHandler(pathArr, info);
        }
        throw new NotFound();   // if no suitable handler
    }

    SegmentMatcher getMatcher() {
        return segMatcher;
    }
}
