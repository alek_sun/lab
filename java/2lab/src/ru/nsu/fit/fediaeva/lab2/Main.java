package ru.nsu.fit.fediaeva.lab2;

import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.ResponseProcessors.FileGetter;
import ru.nsu.fit.fediaeva.lab2.ResponseProcessors.ResponseProcessor;

public class Main {
    /**
     * Create processor and start server
     */
    public static void main(String[] args) {
        Server server = new Server();
        ResponseProcessor responseProcessor = new FileGetter();
        server.setPort(8080);
        try {
            server.start(responseProcessor);
        } catch (ProgramException e) {
            e.printStackTrace();
        }
    }
}
