package ru.nsu.fit.fediaeva.lab2;

import ru.nsu.fit.fediaeva.lab2.Exception.InternalServerError;
import ru.nsu.fit.fediaeva.lab2.Exception.MethodNotAllowed;
import ru.nsu.fit.fediaeva.lab2.Exception.NotFound;
import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.RequestConstructor;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.Response;
import ru.nsu.fit.fediaeva.lab2.ResponseProcessors.ResponseProcessor;

import java.io.IOException;
import java.net.Socket;

/**
 * Get and processes socket
 * Create request and sends response
 */
public class SocketProcessor {
    private Socket socket;

    SocketProcessor(Socket s) {
        socket = s;
    }

    public void start(ResponseProcessor proc) throws ProgramException {
        ResponseSender sender;
        Response resp = new Response();
        try {
            sender = new ResponseSender(socket.getOutputStream());
        } catch (IOException e) {
            throw new ProgramException();
        }
        try {
            RequestConstructor req = new RequestConstructor(socket.getInputStream());
            req.fillRequest();
            ResponseProcessor.checkCorrectRequest(req);
            resp = proc.getResponse(req);
        } catch (IOException | InternalServerError e) { //  incorrect request
            e.printStackTrace();
            resp = proc.createResp(500);
        } catch (MethodNotAllowed e) {
            resp = proc.createResp(405);
        } catch (NotFound e) {
            resp = proc.createResp(404);
        } finally {
            sender.sendResponse(resp);
        }
    }
}

