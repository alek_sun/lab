package ru.nsu.fit.fediaeva.netlab4server.requesthandlers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import ru.nsu.fit.fediaeva.netlab4server.HttpMessage;
import ru.nsu.fit.fediaeva.netlab4server.Message;
import ru.nsu.fit.fediaeva.netlab4server.ServerException;
import ru.nsu.fit.fediaeva.netlab4server.User;

import java.util.ArrayList;

public class UsersGetter implements RequestHandler {
    private final ArrayList<User> users;

    public UsersGetter(ArrayList<User> users) {
        this.users = users;
    }

    @Override
    public HttpMessage getResponse(HttpMessage request) throws ServerException {
        JSONObject userList = new JSONObject();
        JSONArray usersArr = new JSONArray();
        synchronized (users) {
            users.forEach(u -> {
                JSONObject user = new JSONObject();
                user.put("id", u.getId());
                user.put("username", u.getName());
                user.put("online", u.isOnline());
                usersArr.add(user);
            });
        }
        userList.put("users", usersArr);
        String jsonContent = userList.toJSONString();

        HttpMessage response = new HttpMessage();
        response.getHeaders().put("Content-Type", "application/json");
        response.setOKTitle();
        response.setBody(jsonContent);
        return response;
    }
}
