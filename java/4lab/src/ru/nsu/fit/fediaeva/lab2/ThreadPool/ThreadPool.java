package ru.nsu.fit.fediaeva.lab2.ThreadPool;

import java.util.ArrayList;

public class ThreadPool {
    private ArrayList<Runnable> execQueue;
    private ArrayList<ThreadInQueue> threads;

    public ThreadPool(int count) {
        execQueue = new ArrayList<>();
        threads = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            threads.add(new ThreadInQueue(execQueue));
        }
        for (ThreadInQueue t : threads) {
            t.start();
        }
    }

    synchronized public void execute(Runnable r) {
        execQueue.add(r);
        notify();
    }

    synchronized public void stop() {
        for (ThreadInQueue t : threads) {
            t.setStop();
        }
    }
}