package ru.nsu.fit.fediaeva.lab2;

import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.HttpHandler.HttpHandler;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.MatcherTree;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private int port;

    public void setPort(int p) {
        port = p;
    }

    /**
     * Start of server, waiting for socket and their processing by response processor (proc)
     *
     * @throws ProgramException throws if response can't be received
     */
    public void start(MatcherTree tree) throws ProgramException {
        ServerSocket serverSocket;
        try {
            serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new ProgramException();
        }
        Socket s;
        SocketProcessor socketProcessor;
        while (true) {
            try {
                s = serverSocket.accept();
            } catch (IOException e) {
                continue;
            }
            socketProcessor = new SocketProcessor(s, tree);
            try {
                socketProcessor.start();
            } catch (ProgramException ignored) {
            }
        }
    }
}
