package ru.nsu.fit.fedyaeva.template;

public class ArgumetNotFoundException extends Exception {
    private String argument;

    ArgumetNotFoundException(String arg) {
        argument = arg;
    }

    public String what() {
        return "Argument not found: " + argument;
    }
}
