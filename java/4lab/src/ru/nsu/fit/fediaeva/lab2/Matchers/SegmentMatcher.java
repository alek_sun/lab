package ru.nsu.fit.fediaeva.lab2.Matchers;

import java.util.Map;

public interface SegmentMatcher {
    Integer getPriority();

    void setDescr(String d);

    String getDescription();

    String getType();

    boolean match(String segment, Map<String, String> pathInfo);

    boolean equals(SegmentMatcher s);
}
