package ru.nsu.fit.fediaeva.lab2;

import ru.nsu.fit.fediaeva.lab2.Exception.InternalServerError;
import ru.nsu.fit.fediaeva.lab2.Exception.MethodNotAllowed;
import ru.nsu.fit.fediaeva.lab2.Exception.NotFound;
import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.HttpHandler.FileGetter;
import ru.nsu.fit.fediaeva.lab2.HttpHandler.HttpHandler;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.RequestConstructor;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.Response;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.MatcherTree;

import java.io.IOException;
import java.net.Socket;

/**
 * Get and processes socket
 * Create request and sends response
 */
public class SocketProcessor {
    private Socket socket;
    private MatcherTree treeRoot;

    SocketProcessor(Socket s, MatcherTree t) {
        socket = s;
        treeRoot = t;
    }

    public void start() throws ProgramException {
        ResponseSender sender;
        HttpHandler proc = new FileGetter();
        Response resp;
        try {
            sender = new ResponseSender(socket.getOutputStream());
        } catch (IOException e) {
            throw new ProgramException();
        }
        try {
            RequestConstructor req = new RequestConstructor(socket.getInputStream(), treeRoot);
            proc = req.fillRequest();
            HttpHandler.checkRequest(req);
            resp = proc.getResponse(req);
        } catch (IOException | InternalServerError e) { //  if incorrect request
            e.printStackTrace();
            resp = proc.createResp(500);
        } catch (MethodNotAllowed e) {
            resp = proc.createResp(405);
        } catch (NotFound e) {
            resp = proc.createResp(404);
        } catch (ProgramException e) {
            return;
        }
        sender.sendResponse(resp);
    }
}

