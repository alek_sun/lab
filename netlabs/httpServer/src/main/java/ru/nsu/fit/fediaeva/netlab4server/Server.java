package ru.nsu.fit.fediaeva.netlab4server;

import ru.nsu.fit.fediaeva.netlab4server.matchers.IntMatcher;
import ru.nsu.fit.fediaeva.netlab4server.matchers.Matcher;
import ru.nsu.fit.fediaeva.netlab4server.matchers.StringMatcher;
import ru.nsu.fit.fediaeva.netlab4server.requesthandlers.*;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class Server {
    private ArrayList<User> users;
    private ArrayList<Message> messages;
    private ServerSocket socket;


    Server(String host, int port) throws IOException {
        messages = new ArrayList<>();
        users = new ArrayList<>();
        socket = new ServerSocket();
        socket.bind(new InetSocketAddress(host, port));
    }

    void start() {
        ArrayBlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(30);
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS, queue);
        Timer timer = new Timer(users, messages);
        threadPool.execute(timer);
        while (true) {
            try {
                Socket s = socket.accept();
                ResponseSender sender = new ResponseSender(users, messages, s);
                threadPool.execute(sender);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static String readBufferedLine(BufferedInputStream in) throws ServerException {
        ArrayList<Byte> bytes = new ArrayList<>();
        int c;
        while (true) {
            try {
                c = in.read();
            } catch (IOException e) {
                throw new ServerException(500, "Can't read request");
            }
            if (c < 0) {
                return "";
            }
            byte b = (byte) c;
            try {
                bytes.add(b);
            } catch (OutOfMemoryError e) {
                System.out.println(bytes.size());
            }
            if (b == '\n') {
                StringBuilder res = new StringBuilder();
                bytes.remove(bytes.size() - 1);
                if (bytes.get(bytes.size() - 1) == '\r') {
                    bytes.remove(bytes.size() - 1);
                }
                for (Byte bt : bytes) {
                    res.append((char)(byte)bt);
                }
                return res.toString();
            }
        }
    }
}
