import ru.nsu.fit.fediaeva.server.ServerContext;
import ru.nsu.fit.fediaeva.server.ServerPlugin;
import ru.nsu.fit.fediaeva.server.constructors.Request;
import ru.nsu.fit.fediaeva.server.constructors.Response;
import ru.nsu.fit.fediaeva.server.exception.ProgramException;
import ru.nsu.fit.fediaeva.server.handlers.HttpHandler;
import ru.nsu.fit.fediaeva.server.matchers.IntMatcher;
import ru.nsu.fit.fediaeva.server.matchers.MatcherTree;
import ru.nsu.fit.fediaeva.server.matchers.StringMatcher;

import java.util.Arrays;
import java.util.Collections;

public class PlusPlugin implements ServerPlugin {
    @Override
    public void initialize(ServerContext serverContext) throws ProgramException {
        MatcherTree tree = serverContext.getMatcherTree();
        tree.registerHandler(Arrays.asList(
                new StringMatcher("sum", null),
                new IntMatcher("a"),
                new IntMatcher("b")
        ), (HttpHandler) request -> {
            int a = Integer.parseInt(request.getPathInfo().get("a"));
            int b = Integer.parseInt(request.getPathInfo().get("b"));
            Response r = new Response();
            r.setBody(String.valueOf(a+b).getBytes());
            return r;
        }, Collections.emptyList(), Collections.emptyList());
    }

    @Override
    public String getName() {
        return "PlusPlugin";
    }
}
