package ru.nsu.fit.fediaeva.lab2;

import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.Response;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Sends response in OutputStream
 */
public class ResponseSender {
    private OutputStream os;

    ResponseSender(OutputStream out) {
        os = out;
    }

    public void sendResponse(Response resp) throws ProgramException {
        try {
            os.write(("HTTP/1.1 " + resp.getDescription() + "\r\n" + resp.getHeadersString())
                    .getBytes());
            os.write(resp.getBody());
            os.close();
        } catch (IOException e) {
            throw new ProgramException();
        }
    }
}
