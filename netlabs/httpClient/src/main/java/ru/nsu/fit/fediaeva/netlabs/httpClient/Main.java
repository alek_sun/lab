package ru.nsu.fit.fediaeva.netlabs.httpClient;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args.length < 5) {
            System.out.println("Please, enter arguments : <name> <host> <port> <server host> <server port>");
        }
	    Client client = new Client(args[0], args[1], Integer.parseInt(args[2]), args[3], Integer.parseInt(args[4]));
        try {
            client.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
