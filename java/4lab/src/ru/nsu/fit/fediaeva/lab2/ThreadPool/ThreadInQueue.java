package ru.nsu.fit.fediaeva.lab2.ThreadPool;

import java.util.ArrayList;

public class ThreadInQueue extends Thread {
    private ArrayList<Runnable> execQueue;
    private boolean isStopped;

    ThreadInQueue(ArrayList<Runnable> q) {
        execQueue = q;
        isStopped = false;
    }

    public void setStop() {
        isStopped = true;
        interrupt();
    }

    @Override
    public void run() {
        while (!isStopped) {
            synchronized (execQueue) {
                while (execQueue.isEmpty()) {
                    try {
                        execQueue.wait();
                    } catch (InterruptedException ignored) {
                    }
                }
                Runnable task = execQueue.remove(0);
                task.run();
            }
        }
    }
}


