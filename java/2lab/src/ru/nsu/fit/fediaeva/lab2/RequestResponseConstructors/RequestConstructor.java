package ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors;

import ru.nsu.fit.fediaeva.lab2.Exception.InternalServerError;
import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Constructs request from input stream and contains request data
 */
public class RequestConstructor {
    private Map<String, String> headers;
    private byte[] body;
    private BufferedInputStream in;

    public RequestConstructor(InputStream sin) {
        in = new BufferedInputStream(sin);
        headers = new HashMap<>();
    }

    /**
     * Read 1 line from BufferedInputStream
     *
     * @return read line
     * @throws InternalServerError reading error
     */
    private String readBufferedLine() throws InternalServerError, ProgramException {
        List<Byte> l = new ArrayList<>();
        int c;
        while (true) {
            try {
                c = in.read();
            } catch (IOException e) {
                throw new InternalServerError();
            }
            if (c < 0 || c > 255) {
                throw new ProgramException();
            }
            byte b = (byte) c;
            l.add(b);
            if (b == '\n') {
                StringBuilder res = new StringBuilder();
                l.remove(l.size() - 1);
                if (l.get(l.size() - 1) == '\r') {
                    l.remove(l.size() - 1);
                }
                for (Byte j : l) {
                    res.append((char) (byte) j);
                }
                return res.toString();
            }
        }
    }

    /**
     * Get request body
     *
     * @return body in byte array
     * @throws InternalServerError reading error
     */
    public byte[] getBody() throws InternalServerError {
        if (body != null) {
            return body;
        }
        int len = 0;
        if (headers.containsKey("Content-length")) {
            len = Integer.parseInt(headers.get("Content-length"));
        }
        byte[] b = new byte[len];
        try {
            in.read(b, 0, len);
        } catch (IOException e) {
            throw new InternalServerError();
        }
        body = b;
        return b;
    }

    /**
     * Fill path, method, version, headers
     *
     * @throws InternalServerError bad reading
     */
    public void fillRequest() throws InternalServerError, ProgramException {
        getParams(); // 1st line
        getHeaders();
    }

    /**
     * Get method, path and version from 1st line
     *
     * @throws InternalServerError if first request line is incorrect or reading line error
     */
    private void getParams() throws InternalServerError, ProgramException {
        String curStr;
        curStr = readBufferedLine();
        String[] titleList = curStr.split(" ");
        if (titleList.length != 3) {
            throw new InternalServerError();
        }
        String path = titleList[1];
        path = path.replaceFirst("/", "");
        headers.put("Path", path);
        headers.put("Method", titleList[0]);
        headers.put("Version", titleList[2]);
    }

    /**
     * Get and save request headers
     *
     * @throws InternalServerError reading line error
     */
    private void getHeaders() throws InternalServerError, ProgramException {
        while (true) {
            String curStr = readBufferedLine();
            if (curStr.equals("")) {
                break;
            }
            String[] keyVal = curStr.split(": ");
            headers.put(keyVal[0], keyVal[1]);
        }
    }

    /**
     * Get 1 header from request
     */
    public String getHeader(String key) {
        return headers.get(key);
    }

}
