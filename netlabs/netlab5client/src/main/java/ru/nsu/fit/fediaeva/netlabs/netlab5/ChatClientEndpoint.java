package ru.nsu.fit.fediaeva.netlabs.netlab5;

import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;

public class ChatClientEndpoint extends Endpoint {
    private CountDownLatch latch;
    private ArrayBlockingQueue<String> messages;

    ChatClientEndpoint(CountDownLatch latch, ArrayBlockingQueue<String> messages){
        this.latch = latch;
        this.messages = messages;

        final ClientEndpointConfig config = ClientEndpointConfig.Builder.create().build();
        ClientManager client = ClientManager.createClient();

        try {
            client.connectToServer(this, config,
                    new URI("ws://localhost:8025/websockets/login"));
        } catch (URISyntaxException | IOException | DeploymentException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        RemoteEndpoint.Async remote = session.getAsyncRemote();
        remote.sendText("hello");
        session.addMessageHandler(String.class, text ->
                System.out.println("Handler : got your message (" + text + ")"));
    }
}