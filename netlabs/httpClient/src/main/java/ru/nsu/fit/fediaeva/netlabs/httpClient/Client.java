package ru.nsu.fit.fediaeva.netlabs.httpClient;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

class Client {
    private InetSocketAddress serverAddr;
    private ArrayBlockingQueue<String> messages;
    private InetSocketAddress address;
    private String name;
    private AtomicBoolean isStopped;

    Client(String name, String hostname, int port, String serverHost, int serverPort) {
        address = new InetSocketAddress(hostname, port);
        serverAddr = new InetSocketAddress(serverHost, serverPort);
        messages = new ArrayBlockingQueue<>(100);
        this.name = name;
        isStopped = new AtomicBoolean();
    }

    void start() throws IOException {
        Connector connector = new Connector(name, address, serverAddr, messages, isStopped);
        Thread connectorThread = new Thread(connector);
        connectorThread.start();
        String message;
        ConsoleInputReadTask scanner = new ConsoleInputReadTask(isStopped);

        while ((message = scanner.call()) != null) {
            messages.offer(message);
        }
        try {
            connectorThread.join();
        } catch (InterruptedException ignored) {
        }
    }
}
