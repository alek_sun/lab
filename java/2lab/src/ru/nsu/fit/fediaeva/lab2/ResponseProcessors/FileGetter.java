package ru.nsu.fit.fediaeva.lab2.ResponseProcessors;

import ru.nsu.fit.fediaeva.lab2.Exception.InternalServerError;
import ru.nsu.fit.fediaeva.lab2.Exception.NotFound;
import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.RequestConstructor;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.Response;

import java.io.UnsupportedEncodingException;

public class FileGetter implements ResponseProcessor {
    @Override
    public Response getResponse(RequestConstructor req) throws ProgramException {
        String path = req.getHeader("Path");
        FileServer fileServer = new FileServer(path);
        byte[] data;
        try {
            data = fileServer.getData();
        } catch (NotFound nf) {
            return createResp(404);
        } catch (InternalServerError ie) {
            return createResp(500);
        }
        String mime = fileServer.getMimeType();
        return createResp(data, mime, 200);
    }

    /**
     * Creates error response
     * @param code error code
     * @return Error response
     * @throws ProgramException If unsupported encoding
     */
    public Response createResp(Integer code) throws ProgramException {
        try {
            byte[] errBody = ("<b><center style=\"font-size:40px\">"
                    + code + " " +
                    Response.getDescription(code) +
                    "</center></b><br><hr>")
                    .getBytes("Cp1252");
            return createResp(errBody, "text/html", code);
        } catch (UnsupportedEncodingException e) {
            throw new ProgramException();
        }
    }

    /**
     * Create all responses
     * @param data Response body
     * @param mime Data type
     * @param code Response code
     * @return Response
     */
    private Response createResp(byte[] data, String mime, Integer code) {
        Response resp = new Response();
        resp.setCode(code);
        Integer len = data.length;
        resp.setHeader("Content-length", len.toString());
        resp.setHeader("Content-type", mime);
        resp.setHeader("Connection", "close");
        resp.setBody(data);
        return resp;
    }
}
