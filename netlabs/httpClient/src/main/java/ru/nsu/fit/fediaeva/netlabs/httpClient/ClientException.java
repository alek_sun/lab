package ru.nsu.fit.fediaeva.netlabs.httpClient;

public class ClientException extends Exception {
    ClientException(String message){
        super(message);
    }
}
