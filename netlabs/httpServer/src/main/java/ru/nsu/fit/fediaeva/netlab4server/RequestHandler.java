package ru.nsu.fit.fediaeva.netlab4server;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;

public class RequestHandler implements Runnable {
    private Socket socket;
    private ArrayList<Message> messages;
    private ArrayList<User> users;

    RequestHandler(Socket socket,
                          ArrayList<Message> messages,
                          ArrayList<User> connections) {
        this.socket = socket;
        this.messages = messages;
        this.users = connections;
    }

    @Override
    public void run() {
        try {
            HttpMessage request = parseHttpMessage();
            checkMessage(request);
            sendResponse(request);
        } catch (IOException e) {
            System.out.println("Reading request error");
            e.printStackTrace();//////////////////and there too!!
        } catch (ServerException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();/////////////////////////send response!!!
        }
    }

    private void checkMessage(HttpMessage request) throws ServerException {
        if (!request.getHeaders().containsKey("Authorization")){
            throw new ServerException(401, "Authorisation token is absent");
        }
        String token = request
                .getHeaders()
                .get("Authorization")
                .replaceFirst("Token ", "");
        if (!isUserToken(token)) {
            throw new ServerException(403, "Unknown authorisation token");
        }
    }

    private boolean isUserToken(String token) {
        for (User u : users) {
            if (u.getToken().equals(token)){
                return true;
            }
        }
        return false;
    }

    private HttpMessage parseHttpMessage() throws IOException, ServerException {
        BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String firstLine = br.readLine();
        String[] methodPathVersion = firstLine.split(" ", 3);
        if (methodPathVersion.length < 3) {
            throw new ServerException(400, "Bad request title");
        }

        HashMap<String, String> headers = new HashMap<>();
        String line;
        while (!(line = br.readLine()).equals("")){
            String[] kv = line.split(":", 2);
            headers.put(kv[0], kv[1]);
        }
        int len;
        if (headers.containsKey("Content-Length")) {
            len = Integer.parseInt(headers.get("Content-Length"));
        } else {
            throw new ServerException(400, "Content-Length is absent");
        }
        char[] content = new char[len];
        br.read(content, 0, len);// != len ?
        return new HttpMessage(methodPathVersion[0], methodPathVersion[1], headers, new String(content));
    }

    private void sendResponse(HttpMessage request) throws ServerException {
        if (request.getMethod().equals("GET")){
            String path = request.getPath();
            path.replaceFirst("/", "");
            String[] pathArr = path.split("/", 2);
            if (path.equals("users")){
                sendUsersList();
            } else if (pathArr[0].equals("users")) {
                try {
                    Integer id = Integer.parseInt(pathArr[1]);
                    sendUserInfo(id);
                } catch (NumberFormatException e) {
                    throw new ServerException(405, "Incorrect user id");
                }
            } else {
                sendMessageResponse(path);
            }
        }
    }

    private void sendMessageResponse(String path) throws ServerException {
        String[] pathArr = path.split("\\?");   // it's work???
        if (!pathArr[0].equals("messages")){
            throw new ServerException(405, "No such method");
        }
        Integer count;
        Integer offset;
        try {
            String[] offsetCount = pathArr[1].split("&");
        } catch (IndexOutOfBoundsException e){
            throw new ServerException(400, "Incorrect message request : specify offset and count.");
        }
            if (!offsetCount[0].contains("offset=") || !offsetCount[1].contains("count=")){
                throw new ServerException(400, "Incorrect message request : specify offset and count.");
            }
            offset = Integer.parseInt(offsetCount[0].replaceFirst("offset=", ""));
            count = Integer.parseInt(offsetCount[1].replaceFirst("count=", ""));

        JSONObject messageList = new JSONObject();
        JSONArray msgArr = new JSONArray();
        for (int i = offset; i < offset+count && i < messages.size(); i++) {
            Message m = messages.get(i);
            JSONObject msg = new JSONObject();
            msg.put("id", m.getId());
            msg.put("message", m.getData());
            msg.put("author", m.getSenderId());
            msgArr.add(msg);
        }
        messageList.put("messages", msgArr);
        String jsonContent = messageList.toJSONString();
        sendJsonResponse(jsonContent);
    }

    private void sendUserInfo(Integer id) throws ServerException {
        JSONObject userInfo = new JSONObject();
        User user = getUser(id);
        if (user == null) {
            throw new ServerException(404, "No user with such id");
        }
        userInfo.put("id", id);
        userInfo.put("username", user.getName());
        userInfo.put("online", user.isOnline());
        String jsonContent = userInfo.toJSONString();
        sendJsonResponse(jsonContent);
    }

    private void sendJsonResponse(String jsonContent) throws ServerException {
        try {
            BufferedOutputStream out = new BufferedOutputStream(socket.getOutputStream());
            String response =
                    "HTTP/1.1 200 OK\r\n" +
                            "Content-Type: application/json\r\n" +
                            "Content-Length: " + jsonContent.length() +
                            "\r\n\r\n" + jsonContent;
            out.write(response.getBytes(StandardCharsets.UTF_8));
            out.flush();
            out.close();
        } catch (IOException e) {
            throw new ServerException(500, "Writing response error");
        }

    }

    private User getUser(Integer id) {
        for (User u : users) {
            if (u.getId() == id){
                return u;
            }
        }
        return null;
    }

    private void sendUsersList() throws ServerException {
        JSONObject userList = new JSONObject();
        JSONArray usersArr = new JSONArray();
        users.forEach(u -> {
            JSONObject user = new JSONObject();
            user.put("id", u.getId());
            user.put("username", u.getName());
            user.put("online", u.isOnline());
            usersArr.add(user);
        });
        userList.put("users", usersArr);
        String jsonContent = userList.toJSONString();
        sendJsonResponse(jsonContent);
    }
}


