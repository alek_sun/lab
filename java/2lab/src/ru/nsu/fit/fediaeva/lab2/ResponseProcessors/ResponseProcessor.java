package ru.nsu.fit.fediaeva.lab2.ResponseProcessors;

import ru.nsu.fit.fediaeva.lab2.Exception.MethodNotAllowed;
import ru.nsu.fit.fediaeva.lab2.Exception.NotFound;
import ru.nsu.fit.fediaeva.lab2.Exception.ProgramException;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.RequestConstructor;
import ru.nsu.fit.fediaeva.lab2.RequestResponseConstructors.Response;

public interface ResponseProcessor {
    /**
     * Get response by request
     *
     * @param req request
     * @return response
     */
    Response getResponse(RequestConstructor req) throws ProgramException;

    /**
     * Check correctness of request
     *
     * @param req checked request
     * @throws MethodNotAllowed incorrect method
     * @throws NotFound         incorrect path
     */
    static void checkCorrectRequest(RequestConstructor req) throws MethodNotAllowed, NotFound {
        if (!req.getHeader("Method").equals("GET") ||
                !req.getHeader("Version").equals("HTTP/1.1")) {
            throw new MethodNotAllowed();
        }
        if (req.getHeader("Path").endsWith("/")) {
            throw new NotFound();
        }
    }

    /**
     * Error response
     *
     * @param code error code
     * @return error response
     */
    Response createResp(Integer code) throws ProgramException;
}
