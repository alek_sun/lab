#include "gtest\gtest.h"
#include "trit.h"

namespace Lab1 {
    TEST(TritSet, TritSet_TriSetGet_Test) {
        TritSet A(20);
        A[25] = TritSet::Trit::FALSE;
        ASSERT_EQ(A[25], TritSet::Trit::FALSE);
        ASSERT_EQ(A[40], TritSet::Trit::UNKNOWN);
    };

    TEST(TritSet, TritSet_Capaticy_Test){
        TritSet A;
        A[30] = TritSet::Trit::TRUE;
        ASSERT_EQ(A.capacity(), 8);
        TritSet B;
        ASSERT_EQ(B.capacity(), 0);
    }

    TEST(TritSet, Memory_Allocation_Test){
        try {
            TritSet A(20);
        } catch(std::runtime_error err){
            std::cout << err.what();
        }
        catch(std::length_error err){
            std::cout << err.what();
        }
        TritSet B;
        B[45] = TritSet::Trit::UNKNOWN;
        EXPECT_EQ(B.capacity(), 0);
        B[34] = TritSet::Trit::TRUE;
        B[12] = TritSet::Trit::FALSE;
        ASSERT_EQ(B.capacity(), 12);
    }

    TEST(TritSet, Shrink_Length_Test){
        TritSet A;
        A[63] = TritSet::Trit::TRUE;
        ASSERT_EQ(A.capacity(), 16);
        A[25] = TritSet::Trit::FALSE;
        A[63] = TritSet::Trit::UNKNOWN;
        A.shrink();
        ASSERT_EQ(A.capacity(), 8);
        ASSERT_EQ(A.length(), 26);
    }

    TEST(TritSet, Trim_Test){
        TritSet A;
        A[22] = TritSet::Trit::FALSE;
        A[23] = TritSet::Trit::TRUE;
        A[40] = TritSet::Trit::TRUE;
        A.trim(23);
        EXPECT_EQ(A[22], TritSet::Trit::FALSE);
        EXPECT_EQ(A[23], TritSet::Trit::UNKNOWN);
        EXPECT_EQ(A[40], TritSet::Trit::UNKNOWN);
        EXPECT_EQ(A.capacity(), 8);
        EXPECT_EQ(A.length(), 23);
    }

    TEST(TritSet, Cardinality_Test){
        TritSet A;
        A[10] = TritSet::Trit::TRUE;
        A[5] = TritSet::Trit::FALSE;
        A[3] = TritSet::Trit::TRUE;
        A[10] = TritSet::Trit::FALSE;
        EXPECT_EQ(A.cardinality()[TritSet::Trit::FALSE], 2);
        EXPECT_EQ(A.cardinality()[TritSet::Trit::TRUE], 1);
        EXPECT_EQ(A.cardinality()[TritSet::Trit::UNKNOWN], 8);
        TritSet B;
        B[3] = TritSet::Trit::TRUE;
        B[10] = TritSet::Trit::FALSE;
        B[10] = TritSet::Trit::UNKNOWN;
        EXPECT_EQ(B.cardinality()[TritSet::Trit::UNKNOWN], 3);
    }

    TEST(TritSet, Logic_Test){
        TritSet A, B, C;
        A[5] = TritSet::Trit::TRUE;
        A[10] = TritSet::Trit::FALSE;
        B[5] = TritSet::Trit::FALSE;
        B[3] = TritSet::Trit::TRUE;
        C = A & B;
        EXPECT_EQ(C[10], TritSet::Trit::FALSE);
        EXPECT_EQ(C[5], TritSet::Trit::FALSE);
        EXPECT_EQ(C[3], TritSet::Trit::UNKNOWN);
        C = A | B;
        EXPECT_EQ(C[10], TritSet::Trit::UNKNOWN);
        EXPECT_EQ(C[5], TritSet::Trit::TRUE);
        EXPECT_EQ(C[3], TritSet::Trit::TRUE);
        C = !A;
        EXPECT_EQ(C[10], TritSet::Trit::TRUE);
        EXPECT_EQ(C[5], TritSet::Trit::FALSE);
        EXPECT_EQ(C[3], TritSet::Trit::UNKNOWN);
    }

    TEST(TritHolder, TritHolder_Operator){
        TritSet A(20);
        A[1] = TritSet::Trit::TRUE;
        A[3] = A[1];
        ASSERT_EQ(A[3], A[1]);
    }

    TEST(TritHolder, TritHolder_Logic_Test){
        TritSet A(20), B(20), C(20);
        B[10] = TritSet::Trit::TRUE;
        C[10] = TritSet::Trit::FALSE;
        A[1] = B[10] | C[10];
        A[2] = B[10] & C[10];
        A[3] = !B[10];
        EXPECT_EQ(A[2], TritSet::Trit::FALSE);
        EXPECT_EQ(A[3], TritSet::Trit::FALSE);
        EXPECT_EQ(A[1], TritSet::Trit::TRUE);
        A[5] = A[20];
        EXPECT_EQ(A[5], TritSet::Trit::UNKNOWN);
    }

    TEST(TritHolder, TritHolder_3logic_Test){
        TritSet A(20), B(20), C(20);
        B[10] = TritSet::Trit::TRUE;
        C[10] = TritSet::Trit::FALSE;
        A[10] = B[10] | C[10] & TritSet::Trit::TRUE;
        EXPECT_EQ(A[10], TritSet::Trit::TRUE);
        A[5] = B[5] | C[10] & TritSet::Trit::UNKNOWN;
        EXPECT_EQ(A[5], TritSet::Trit::UNKNOWN);
    }

  /*  TEST(TritSet, load_save){
        TritSet A;
        A.loadFromStream(std::cin);
        A.saveToStream(std::cout);
    }*/
}
    int main(int argc, char** argv) {
        ::testing::InitGoogleTest(&argc, argv);
        return  RUN_ALL_TESTS();
    };

