package ru.nsu.fit.fediaeva.netlabs.httpClient;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class Connector implements Runnable {
    private URL url;
    private InetSocketAddress serverAddr;
    private InetSocketAddress address;
    private AtomicBoolean isStopped;
    private ArrayBlockingQueue<String> messages;
    private String token;
    private String login;
    private HttpURLConnection connection;


    Connector(String login, InetSocketAddress addr, InetSocketAddress serverAddr,
              ArrayBlockingQueue<String> messages, AtomicBoolean isStopped) {
        this.serverAddr = serverAddr;
        address = addr;
        token = "";
        this.login = login;
        this.isStopped = isStopped;
        this.messages = messages;
    }

    private void stop() {
        isStopped.set(true);
    }

    @Override
    public void run() {
        try {
            url = new URL("http", serverAddr.getHostName(), serverAddr.getPort(), "/login");
        } catch (MalformedURLException e) {
            System.out.println("Incorrect URL");
            return;
        }

        if (!authorise()){
            stop();
            return;
        }
        Updater updater = new Updater(address.getHostName(), address.getPort(), serverAddr, isStopped, token);
        Thread updaterThread = new Thread(updater);
        updaterThread.start();
        while (!isStopped.get()) {
            String msg;
            try {
                msg = messages.take();
                if (!safeSend(msg)) {
                    stop();
                    return;
                }
                try {
                    handleResponse(msg);
                } catch (ClientException e) {
                    stop();
                    return;
                }
            } catch (InterruptedException e) {
                safeSend("-exit");
                try {
                    handleResponse("-exit");
                } catch (ClientException ignored) {}
                stop();
                return;
            }

        }
    }

    private void handleResponse(String request) throws ClientException {
        try {
            int respCode = connection.getResponseCode();
            switch (respCode) {
                case (HttpsURLConnection.HTTP_OK): {
                    String respBody = readResponse();
                    try {
                        printResponse(respBody, request);
                    } catch (ParseException e) {
                        throw new ClientException("ParseException");
                    }
                    break;
                }
                case (HttpsURLConnection.HTTP_FORBIDDEN): {
                    System.out.println(connection.getResponseMessage());
                    if (!authorise()) {
                        throw new ClientException("Authorisation failed");
                    }
                    break;
                }
                default: {
                    System.out.println(connection.getResponseMessage());
                }
            }
        } catch (IOException e){
            System.out.println("Server is not available");
        }
    }

    private void printResponse(String responseBody, String request) throws ParseException {
        if (responseBody.equals("")) {
            return;
        }
        JSONParser jsonParser = new JSONParser();
        JSONObject body = (JSONObject) jsonParser.parse(responseBody);
        switch (request) {
            case ("-exit") : {
                System.out.println(body.get("message"));
                break;
            }
            case ("-users") : {
                JSONArray users = (JSONArray) body.get("users");
                System.out.println("============ USER LIST =============");
                for (Object u : users) {
                    System.out.println(u);
                }
                break;
            }
            case ("-messageList") : {
                JSONArray messages = (JSONArray) body.get("messages");
                System.out.println("============ MESSAGES =============");
                for (Object message : messages) {
                    JSONObject msg = (JSONObject) message;
                    System.out.println(msg.get("author") + " : " + msg.get("message"));
                }
                break;
            }
            default : {
                System.out.println(responseBody);
            }
        }
    }

    private boolean safeSend(String message){
        try {
            send(message);
        } catch (IOException e){
            System.out.println("Sending message error. Resending...");
            try {
                send(message);
            } catch (IOException e1) {
                System.out.println("Sending message error");
                return false;
            }
        }
        return true;
    }

    private void send(String msg) throws IOException {
        switch (msg) {
            case ("-exit") : {
                isStopped.set(true);
                sendJSONMessage("", "POST", "/logout");
                break;
            }
            case ("-users"): {
                sendJSONMessage("", "GET", "/users");
                break;
            }
            case ("-messageList") : {
                sendJSONMessage("", "GET", "/messages");
                break;
            }
            default : {
                sendJSONMessage(createUsualMsg(msg), "POST", "/messages");
            }
        }
    }

    private String createUsualMsg(String msg) {
        JSONObject jsonContent = new JSONObject();
        jsonContent.put("message", msg);
        return jsonContent.toJSONString();
    }

    private void sendJSONMessage(String jsonContent, String method, String path) throws IOException {
        url = new URL("http", serverAddr.getHostName(), serverAddr.getPort(), path);
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setRequestProperty("Authorization", token);
        connection.setRequestProperty("Host", address.getHostName() + ":" + address.getPort());
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);
        if (jsonContent.equals("")) return;
        //WARNING! this change method to POST
        BufferedOutputStream os = new BufferedOutputStream(connection.getOutputStream());
        os.write(jsonContent.getBytes(StandardCharsets.UTF_8));
        os.flush();
        os.close();
    }

    private boolean authorise(){
        try {
            sendLoginRequest();
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String response = readResponse();
                JSONParser jsonParser = new JSONParser();
                try {
                    JSONObject responseBody = (JSONObject) jsonParser.parse(response);
                    token = (String) responseBody.get("token");
                    System.out.println("Success authorised by name " + responseBody.get("username"));
                    System.out.println("Your id : " + responseBody.get("id"));
                    showHelp();
                } catch (ParseException e) {
                    System.out.println("Parser error");
                }
            } else if (responseCode == HttpsURLConnection.HTTP_UNAUTHORIZED) {
                System.out.println("This name is already in use, please use other name");
                return false;
            } else {
                System.out.print("Connection error : ");
                System.out.println(connection.getResponseMessage());
                return false;
            }
        } catch (IOException e){
            System.out.println("Connection error. Server is not available");
            return false;
        }
        return true;
    }

    private void showHelp() {
        System.out.println("Available commands : ");
        System.out.println("[ -messageList ] shows the list of messages");
        System.out.println("[ -users ] shows the list of users");
        System.out.println("[ -exit ]");
    }

    private String readResponse() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(
                connection.getInputStream(), StandardCharsets.UTF_8));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    private void sendLoginRequest() throws IOException {
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Host", address.getHostName() + ":" + address.getPort());
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(true);

        JSONObject jsonContent = new JSONObject();
        jsonContent.put("username", login);
        String bodyStr = jsonContent.toJSONString();
        connection.setRequestProperty("Content-Length", String.valueOf(bodyStr.length()));
        BufferedOutputStream os = new BufferedOutputStream(connection.getOutputStream());
        os.write(bodyStr.getBytes(StandardCharsets.UTF_8));
        os.flush();
        os.close();
    }
}
