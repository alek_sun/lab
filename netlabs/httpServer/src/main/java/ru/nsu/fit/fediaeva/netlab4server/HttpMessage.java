package ru.nsu.fit.fediaeva.netlab4server;

import java.util.HashMap;

public class HttpMessage {
    private String method;
    private String path;
    private final HashMap<String, String> headers;
    private String body;
    private String title;

    public HttpMessage(){
        method = "";
        path = "";
        headers = new HashMap<>();
        headers.put("Content-Length", String.valueOf(0));
        body = "";
        title = "";
    }

    public HttpMessage(String method, String path, HashMap<String, String> headers, String body) {
        this.method = method;
        this.path = path;
        this.headers = headers;
        headers.put("Content-Length", String.valueOf(body.length()));
        this.body = body;
        title = method + " " + path + " " + "HTTP/1.1";
    }

    public HttpMessage(String title, HashMap<String, String> headers, String body){
        this.title = title;
        this.headers = headers;
        this.body = body;
    }

    String getTitle() {
        return title.equals("") ? method + " " + path + " " + "HTTP/1.1" : title;
    }

    public String getMethod() {
        return method;
    }

    public String getPath() {
        return path;
    }

    public HashMap<String, String> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }

    void setMethod(String method) {
        this.method = method;
    }

    void setPath(String path) {
        this.path = path;
    }

    public void setBody(String body) {
        this.body = body;
        headers.put("Content-Length", String.valueOf(body.length()));
    }

    String getHeadersString() {
        StringBuilder res = new StringBuilder();
        headers.forEach((k, v) -> res.append(k).append(": ").append(v).append("\r\n"));
        res.append("\r\n");
        return res.toString();
    }

    public String getToken(){
        String auth = headers.get("Authorization");
        if (auth != null){
            return auth.replaceFirst("Token ", "");
        } else {
            return null;
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setOKTitle(){
        title = "HTTP/1.1 200 OK";
    }

    public void print(){
        System.out.println(getTitle());
        System.out.println(getHeadersString());
        System.out.println(body);
    }
}
