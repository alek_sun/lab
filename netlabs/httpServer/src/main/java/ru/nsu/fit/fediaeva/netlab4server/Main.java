package ru.nsu.fit.fediaeva.netlab4server;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Server server = null;
        try {
            server = new Server("localhost", 8080);
        } catch (IOException e) {
            e.printStackTrace();
        }
        server.start();
    }
}
