package ru.nsu.fit.fediaeva.netlab4server;

public class Message {
    private static long globalId;
    private long id;
    private String data;
    private long senderId;

    public Message(String data, long senderId) {
        ++globalId;
        id = globalId;
        this.data = data;
        this.senderId = senderId;
    }

    public String getData() {
        return data;
    }

    public long getId() {
        return id;
    }

    public long getSenderId() {
        return senderId;
    }
}
