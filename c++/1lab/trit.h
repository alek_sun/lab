#ifndef INC_1LAB_TRIT_H
#define INC_1LAB_TRIT_H

#include <unordered_map>

/*
 *  Methods:
    void loadFromStream(std::istream&)
    input format: T|count of trits|uint1 uint2 uint3 etc.
    int length()- logical length
    int capacity() - number of bytes in container
    void shrink() - cut unused trits
    void trim(int) - cut from index
    TrMap cardinality() - map for count of TRUE, FALSE, UNKNOWN
    int cardinality(Trit) - count of TRUE, FALSE or UNKNOWN
    void saveToStream(std::ostream&) - output format --//--
    TritIterator begin() - first uint
    TritIterator end() - last uint + 1
 * */

namespace Lab1 {
    class TritHolder;
    class TritSet {
    public:
        class TritHolder;
        class TritIterator;
        class CTritIterator;
        class UintTrit; // hash

        //Friends
        friend class TritHolder;

        enum class Trit {
            TRUE = 1,
            UNKNOWN = 0,
            FALSE = -1,
        };

        enum LogicOperator{
            AND = 6,
            OR = 9,
            NOT = 7,
        };

        typedef unsigned int uint;
        typedef std::unordered_map<Trit, int, UintTrit> TrMap;

        //Methods
        int length();
        int capacity();
        void shrink();
        void trim(int);
        TrMap cardinality();
        int cardinality(Trit);
        void loadFromStream(std::istream&);
        void saveToStream(std::ostream&);
        TritIterator begin();
        TritIterator end();

        //Constructors
        TritSet(); // empty member
        TritSet(const TritSet &basic);  // copy constructor
        TritSet(int size); // by count of elements

        //Destructors
        ~TritSet();

        //Operators
        TritSet& operator=(const TritSet &); // copy operator
        TritHolder operator[](int);
        Trit operator[](int) const;
        TritSet operator& (TritSet&);
        TritSet operator|(TritSet&);
        TritSet operator!();

    private:
        //Variables
        uint *cont2B; // uint array
        int num2B; // number of trits
        int countTRUE; // cout of TRUE
        int countFALSE; // count of FALSE
        int lastFixInd; // last index of not unknown
        int arrSize; // sizeof cont2B

        //Methods
        void set(int trIndex, Trit value); // set trit to index
        Trit get(int trIndex) const; // get trit by index
        TritSet logic(int, TritSet&);
        static Trit handleBinaryOperation(int, Trit, Trit);
        static Trit notOperation(Trit);
        int trueCount();
        int falseCount();
        int unkCount();
        void resize(int num2B); // resize for num2B trits

        //Constants
        /*static const int AND = 6;
        static const int OR = 9;
        static const int NOT = 7;*/
        static const int BIT_ON_BYTE = 8;
        static const int BIT2 = 2;
        static const int RADIX16 = 16;
        static const int RADIX4 = 4;
        static const int ZERO = 0;
        static const int ONE = 1;
        static const int SECT2B = sizeof(int) * BIT_ON_BYTE / BIT2; // count of 2bit in one section of cont2B
    };

  class TritSet::TritIterator {
    public:
        TritIterator(TritSet&, int);
        TritSet::TritHolder operator*();
        TritIterator& operator++();
        TritIterator& operator++(int);
        bool operator==(TritIterator);
        bool operator!=(TritIterator);
    private:
        TritSet& set;
        int ind; // buf index of trit
    };

    class TritSet::CTritIterator {
    public:
        CTritIterator(TritSet&, int);
        TritSet::TritHolder operator*() const;
        CTritIterator& operator++();
        CTritIterator& operator++(int);
        bool operator==(CTritIterator);
        bool operator!=(CTritIterator);
    private:
        TritSet& set;
        int ind; // buf index of trit
    };

    class TritSet::UintTrit{
    public:
        //Constructor
        UintTrit();
        // Operator Trit to uint
        uint operator()(const TritSet::Trit&) const;
    };

    class TritSet::TritHolder {
    public:
        //Constructor
        TritHolder(TritSet&, int);

        //Operators
        TritSet::Trit operator=(TritSet::Trit);// assigment
        TritSet::Trit operator=(TritHolder);// copy
        operator TritSet::Trit() const;
        TritSet::Trit operator&(TritSet::Trit);
        TritSet::Trit operator|(TritSet::Trit);
        TritSet::Trit operator!();
    private:
        TritSet &rootCont;
        int ind; // buf index
    };
}


#endif //INC_1LAB_TRIT_H
