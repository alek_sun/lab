package ru.nsu.fit.fediaeva.netlab4server;

import ru.nsu.fit.fediaeva.netlab4server.matchers.IntMatcher;
import ru.nsu.fit.fediaeva.netlab4server.matchers.Matcher;
import ru.nsu.fit.fediaeva.netlab4server.matchers.StringMatcher;
import ru.nsu.fit.fediaeva.netlab4server.requesthandlers.*;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static ru.nsu.fit.fediaeva.netlab4server.Server.readBufferedLine;

public class ResponseSender implements Runnable {
    private BufferedInputStream in;
    private BufferedWriter outputStream;
    private final ArrayList<User> users;
    private ArrayList<Message> messages;
    private HandlersTree handlersTree;
    private HttpMessage request;

    ResponseSender(ArrayList<User> users, ArrayList<Message> messages, Socket socket) throws IOException {
        this.users = users;
        this.messages = messages;
        handlersTree = new HandlersTree();
        fillHandlersTree();
        in = new BufferedInputStream(socket.getInputStream());
        outputStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
    }

    @Override
    public void run() {
        request = new HttpMessage();
        RequestHandler handler;
        try {
            handler = findHandler(in, request);
            parseHttpMessage();
            updateUserTime();
            checkMessage();
            request.print();
            sendJsonResponse(handler.getResponse(request));
        } catch (ServerException e) {
            System.out.println("SERVER ERR");
            ErrorResponseSender errSender = new ErrorResponseSender(
                    e.getTitle(),
                    e.getInfo(),
                    outputStream);
            errSender.run();
        }
    }

    private void updateUserTime() {
        synchronized (users) {
            for (User u : users) {
                if (u.getToken().equals(request.getToken())){
                    u.setLastMsgTime(System.currentTimeMillis());
                }
            }
        }
    }

    private void checkMessage() throws ServerException {
        if (!request.getPath().equals("login")) {
            if (!request.getHeaders().containsKey("Authorization")) {
                throw new ServerException(401, "Authorisation token is absent");
            }
            String token = request.getToken();
            if (!isUserToken(token)) {
                throw new ServerException(403, "Unknown authorisation token");
            }
        }
        if (!request.getHeaders().get("Content-Type").equals("application/json")) {
            throw new ServerException(400, "Unknown message format");
        }
    }

    private boolean isUserToken(String token) {
        synchronized (users) {
            for (User u : users) {
                if (u.getToken().equals(token)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void parseHttpMessage() throws ServerException {
        HashMap<String, String> headers = new HashMap<>();
        String line;
        while (!(line = readBufferedLine(in)).equals("")) {
            String[] kv = line.split(":", 2);
            headers.put(kv[0], kv[1].replaceFirst(" ", ""));
        }
        int len;
        request.getHeaders().putAll(headers);
        if (request.getHeaders().containsKey("Content-Length")) {
            len = Integer.parseInt(request.getHeaders().get("Content-Length"));
        } else {
            throw new ServerException(400, "Content-Length is absent");
        }
        byte[] contentBuf = new byte[len];
        int n;
        try {
            n = in.read(contentBuf, 0, len);
        } catch (IOException e) {
            throw new ServerException(500, "Reading body error");
        }
        if (n != len) {
            throw new ServerException(500, "Not whole request body has been read");
        }
        request.setBody(new String(contentBuf, StandardCharsets.UTF_8));
    }

    private void sendJsonResponse(HttpMessage response) throws ServerException {
        try {
            outputStream.write(response.getTitle() + "\r\n");
            outputStream.write(response.getHeadersString());
            outputStream.write(response.getBody());
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new ServerException(500, "Writing response error");
        }

    }

    private void fillHandlersTree() {
        ArrayList<Matcher> postMsg = new ArrayList<>() {{
            add(new StringMatcher("POST"));
            add(new StringMatcher("messages"));
        }};
        handlersTree.addHandler(postMsg.iterator(), new AddMessage(users, messages));

        ArrayList<Matcher> getMessages = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("messages"));
        }};
        handlersTree.addHandler(getMessages.iterator(), new MessagesGetter(messages));

        ArrayList<Matcher> getCountMessages = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("messages"));
            add(new StringMatcher("count="));
        }};
        handlersTree.addHandler(getCountMessages.iterator(), new CountMessageGetter(messages));

        ArrayList<Matcher> getCountOffMessages = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("messages"));
            add(new StringMatcher("offset="));
            add(new StringMatcher("count="));
        }};
        handlersTree.addHandler(getCountOffMessages.iterator(), new OffCountMessageGetter(users, messages));

        ArrayList<Matcher> getOffMessages = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("messages"));
            add(new StringMatcher("offset="));
        }};
        handlersTree.addHandler(getOffMessages.iterator(), new OffsetMessagesGetter(messages));

        ArrayList<Matcher> getUsers = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("users"));
        }};
        handlersTree.addHandler(getUsers.iterator(), new UsersGetter(users));

        ArrayList<Matcher> getIdUser = new ArrayList<>() {{
            add(new StringMatcher("GET"));
            add(new StringMatcher("users"));
            add(new IntMatcher());
        }};
        handlersTree.addHandler(getIdUser.iterator(), new IdUserGetter(users));

        ArrayList<Matcher> login = new ArrayList<>() {{
            add(new StringMatcher("POST"));
            add(new StringMatcher("login"));
        }};
        handlersTree.addHandler(login.iterator(), new LoginHandler(users, messages));

        ArrayList<Matcher> logout = new ArrayList<>() {{
            add(new StringMatcher("POST"));
            add(new StringMatcher("logout"));
        }};
        handlersTree.addHandler(logout.iterator(), new LogoutHandler(users, messages));
    }

    private RequestHandler findHandler(BufferedInputStream in, HttpMessage m) throws ServerException {
        String firstLine;
        firstLine = readBufferedLine(in);
        System.out.println(firstLine);
        String[] methodPathVersion = firstLine.split(" ", 3);
        ArrayList<String> pathForTree = new ArrayList<>();

        String method = methodPathVersion[0];
        m.setMethod(method);
        pathForTree.add(method);
        String path = methodPathVersion[1];
        path = path.replaceFirst("/", "");
        m.setPath(path);

        String[] pathParts = path.split("[&?/]");

        // "count=<count>" => "count="
        for (int i = 0; i < pathParts.length; i++) {
            String[] elTokens = pathParts[i].split("=");
            if (elTokens.length == 2) {
                pathParts[i] = elTokens[0] + "=";
            }
        }
        ArrayList<String> partsList = new ArrayList<>(Arrays.asList(pathParts));
        pathForTree.addAll(partsList);
        RequestHandler handler = handlersTree.getHandler(pathForTree.iterator());
        if (handler == null) {
            throw new ServerException(405, "No such method");
        }
        return handler;
    }

}


