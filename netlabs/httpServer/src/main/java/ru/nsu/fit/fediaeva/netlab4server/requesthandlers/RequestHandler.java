package ru.nsu.fit.fediaeva.netlab4server.requesthandlers;

import ru.nsu.fit.fediaeva.netlab4server.HttpMessage;
import ru.nsu.fit.fediaeva.netlab4server.Message;
import ru.nsu.fit.fediaeva.netlab4server.ServerException;
import ru.nsu.fit.fediaeva.netlab4server.User;

import java.util.ArrayList;

public interface RequestHandler {
    HttpMessage getResponse(HttpMessage request) throws ServerException;
}
