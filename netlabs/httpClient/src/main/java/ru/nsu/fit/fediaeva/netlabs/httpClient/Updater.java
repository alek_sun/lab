package ru.nsu.fit.fediaeva.netlabs.httpClient;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Updater implements Runnable {
    private InetSocketAddress serverAddr;
    private int port;
    private String host;
    private AtomicBoolean isStopped;
    private HttpURLConnection connection;
    private String token;
    private int offset;

    Updater(String host, int port, InetSocketAddress serverAddress, AtomicBoolean isStopped, String token) {
        this.host = host;
        this.port = port;
        this.serverAddr = serverAddress;
        this.isStopped = isStopped;
        this.token = token;
        offset = 0;
    }

    void stop(){
        isStopped.set(true);
    }

    @Override
    public void run() {
        while (!isStopped.get()){
            try {
                sendUpdateMsg();
                receiveMessage();
            } catch (IOException e) {
                System.out.println("Updater : Server isn't available. Reconnection...");
                try {
                    sendUpdateMsg();
                    receiveMessage();
                } catch (IOException e1) {
                    System.out.println("Updater : Server isn't available. Stopping...");
                    stop();
                }
            }
        }
    }

    private void receiveMessage() throws IOException {
        int code = connection.getResponseCode();
        connection.setReadTimeout(0);
        if (code == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONParser jsonParser = new JSONParser();
            JSONObject body;
            try {
                body = (JSONObject) jsonParser.parse(response.toString());
            } catch (ParseException e) {
                System.out.println("Parse json error");
                return;
            }
            JSONArray messages = (JSONArray) body.get("messages");
            offset += messages.size();
            for (Object message : messages) {
                JSONObject msg = (JSONObject) message;
                System.out.println(msg.get("author") + " : " + msg.get("message"));
            }
        } else {
            System.out.println(connection.getResponseMessage());
        }
    }

    private void sendUpdateMsg() throws IOException {
        try {
            URL url = new URL("http", serverAddr.getHostName(), serverAddr.getPort(),
                    "/messages?offset=" + offset + "&count=1");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", token);
            connection.setRequestProperty("Host", host + ":" + port);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", "0");
            connection.setDoOutput(true);
        } catch (MalformedURLException | ProtocolException e) {
            System.out.println(e.getMessage());
        }
    }

}
